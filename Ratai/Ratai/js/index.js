var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope) {

  $scope.productsList = [{
      product_id: '1',
      product_name: 'Product 1',
      product_price: 100,
      product_image: 'images/1.png',
      product_button: 'Add to cart',
    },
    {
      product_id: '2',
      product_name: 'Product 2',
      product_price: 150,
      product_image: 'images/2.png',
      product_button: 'Add to cart',
    },
    {
      product_id: '3',
      product_name: 'Product 3',
      product_price: 200,
      product_image: 'images/3.png',
      product_button: 'Add to cart',
    },
    {
      product_id: '4',
      product_name: 'Product 4',
      product_price: 250,
      product_image: 'images/4.png',
      product_button: 'Add to cart',
    },
    {
      product_id: '5',
      product_name: 'Product 5',
      product_price: 300,
      product_image: 'images/5.png',
      product_button: 'Add to cart',
    },
    {
      product_id: '6',
      product_name: 'Product 6',
      product_price: 350,
      product_image: 'images/6.png',
      product_button: 'Add to cart',
    },
    {
      product_id: '7',
      product_name: 'Product 7',
      product_price: 400,
      product_image: 'images/7.png',
      product_button: 'Add to cart',
    },
    {
      product_id: '8',
      product_name: 'Product 8',
      product_price: 450,
      product_image: 'images/8.png',
      product_button: 'Add to cart',
    },
    {
      product_id: '9',
      product_name: 'Product 9',
      product_price: 500,
      product_image: 'images/9.png',
      product_button: 'Add to cart',
    },
    {
      product_id: '10',
      product_name: 'Product 10',
      product_price: 550,
      product_image: 'images/10.png',
      product_button: 'Add to cart',
    },
    {
      product_id: '11',
      product_name: 'Product 11',
      product_price: 600,
      product_image: 'images/11.png',
      product_button: 'Add to cart',
    },
    {
      product_id: '12',
      product_name: 'Product 12',
      product_price: 650,
      product_image: 'images/12.png',
      product_button: 'Add to cart',
    },
  ];


  $scope.cartNumber = 0;

  $scope.cartItems = [];
  $scope.addTocart = function(item) {
    $scope.cartItems.push($scope.productsList[item]);
    $scope.cartNumber = $scope.cartItems.length;
    $scope.productsList[item].product_disable = true;
    $scope.productsList[item].product_button = 'Added';
  };

  $scope.removeCart = function(item) {
    $scope.cartItems[item].product_disable = false;
    $scope.cartItems[item].product_button = 'Add to cart';
    $scope.cartItems.splice(item, 1);
    $scope.cartNumber = $scope.cartItems.length;
  };

  $scope.cartTotal = function() {
    var total = 0;
    angular.forEach($scope.cartItems, function(value) {
      total = total + value.product_price;
    });
    return total;
  };







});
